const fs = require("fs");
const data = "/home/bhuvankarthi/callbackDrills/sample.json";

function problem1() {
    let result = []

    fs.readFile(data, "utf-8", (err, file) => {
        if (err) {
            console.log(err);
        } else {
            try {
                const result = (JSON.parse(file))
                fs.mkdir("../answers", function (err) {
                    if (err) {
                        console.log(err);
                    } else {
                        console.log("answers directory created");
                        fs.writeFile("../answers/problem1.json", JSON.stringify(result), (err) => {
                            if (err) {
                                console.log(err);
                            }
                            else {
                                console.log("file created");
                                fs.unlink("../answers/problem1.json", function (err) {
                                    if (err) {
                                        console.log(err);
                                    } else {
                                        console.log("file deleted");
                                    }
                                })
                            }
                        })
                    }
                })

            } catch (err) {
                console.log(err);
            }
        }
    })



}

module.exports = problem1


